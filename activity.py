name = "John Anthony E. Torrejas"
age = "23"
occupation = "Student"
movie = "Doctor Strange in the Multiverse of Madness"
rating = "99.99"

print("I am "+name +", and I am "+ age + " years old, I work as a "+ occupation
+", and my rating for " + movie +" is " + rating +"%"
)

num1, num2, num3 = 1, 2, 3
print(num1 + num2)
print(num1 < num3)
print(num3 + num2)